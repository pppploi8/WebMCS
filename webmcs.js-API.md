函数名称 [webmcs.login.getUsername]  
函数介绍: 获取登录用户的用户名，可用于判断登录状态  
参数 - callback 回调函数，如果不填写则使用同步请求，返回请求结果。如果填写则发送异步请求，通过回调函数传回调用结果，传回结果同函数返回值一致  
返回值: 返回{status: status}对象，如果登录成功status=true，且额外返回name字段，登录失败返回false  

---

函数名称 [webmcs.login.logout]  
函数介绍: 注销登录状态，无返回值，调用后请自行刷新登录状态  
参数 - callback 回调函数，如果不填写则使用同步请求，返回请求结果。如果填写则发送异步请求，通过回调函数传回调用结果，传回结果同函数返回值一致  

---

函数名称 [webmcs.login.isInit]  
函数介绍: 判断是否需要初始化(注册还是登录)  
参数 - callback 回调函数，如果不填写则使用同步请求，返回请求结果。如果填写则发送异步请求，通过回调函数传回调用结果，传回结果同函数返回值一致  
返回值: 返回{isfirst: isfirst}对象，如果是第一次初始化值为true，否则为false  

---

函数名称 [webmcs.login.login]  
函数介绍: 根据账号密码进行登录  
参数 - name 用户名  
参数 - pwd 密码  
参数 - code 验证码，可以通过login/image.login接口获取验证码图片  
参数 - callback 回调函数，如果不填写则使用同步请求，返回请求结果。如果填写则发送异步请求，通过回调函数传回调用结果，传回结果同函数返回值一致  
返回值: 返回{status: status, msg: msg}对象，如果调用成功则status=true，失败返回false且返回msg字段。可能的值有  
*		  CHECKERROR   = 账号密码错误  
*		  CODEERROR    = 验证码错误  
*		  ALREADYLOGIN = 已登录  
*	      NETERROR	   = 网络错误接口调用失败  
*		  如果返回null，说明接口调用成功  

---

函数名称 [webmcs.login.register]  
函数介绍: 注册账号，仅能使用一次，可根据webmcs.login.isInit接口判断是否可以注册  
参数 - name 用户名  
参数 - pwd 密码  
参数 - callback 回调函数，如果不填写则使用同步请求，返回请求结果。如果填写则发送异步请求，通过回调函数传回调用结果，传回结果同函数返回值一致  
返回值: 返回{status: status, msg: msg}对象，如果调用成功则status=true，失败返回false且返回msg字段。可能的值有  
*	      ALREADYINIT = 已初始化(已注册过账号)  
*        PARAMERROR  = 传递参数错误  
*	      NETERROR    = 网络错误接口调用失败  
*	      如果返回null，说明接口调用成功  

---

函数名称 [webmcs.server.getMessage]  
函数介绍: 获取服务器回显消息  
参数 - uuid 消息uuid，可以不填写。如果不填写则拉取服务器缓冲区中所有消息  
参数 - callback 回调函数，如果不填写则使用同步请求，返回请求结果。如果填写则发送异步请求，通过回调函数传回调用结果，传回结果同函数返回值一致  
返回值: 执行成功时返回 {message: message, uuid: uuid, status: status} 对象  
* message为服务器拉取的回显消息  
* uuid为最后一条记录的uuid，用于传递给本接口再次重复调用  
* status为接口请求状态，true=服务器正在运行，false=服务器不在允许或账号未登录。如果是未登录则额外返回msg=NOLOGIN  

---

函数名称 [webmcs.server.start]  
函数介绍: 开启MC服务器  
参数 - callback 回调函数，如果不填写则使用同步请求，返回请求结果。如果填写则发送异步请求，通过回调函数传回调用结果，传回结果同函数返回值一致  
返回值: 返回{status: status,msg: msg}对象，成功时status=true，且无msg字段，失败时msg字段如下  
*        NOTLOGIN      = 没有登录账号，接口调用失败  
*        ALREADYSERVER = 服务器已运行，无法重复运行  
*        JARERROR      = JAR文件不存在或者未设置  
*        PROCESSERROR  = 进程创建失败，一般是Java或者启动命令错误导致的  
*        NETERROR      = 网络错误，接口请求失败  

---

函数名称 [webmcs.server.stop]  
函数介绍: 通过发送stop指令来关闭MC服务器  
参数 - callback 回调函数，如果不填写则使用同步请求，返回请求结果。如果填写则发送异步请求，通过回调函数传回调用结果，传回结果同函数返回值一致  
返回值: 返回{status: status,msg: msg}对象，成功时status=true，且无msg字段，失败时msg字段如下  
*        NOTLOGIN      = 没有登录账号，接口调用失败  
*        SERVERNOTRUN  = 服务器不在运行，无法关闭  
*        UNKNOWNERROR  = 后台IO异常，一般不会出现这个错误。如果出现请检查报错并反馈给作者改进  
*        NETERROR      = 网络错误，接口请求失败  

---

函数名称 [webmcs.server.termination]  
函数介绍: 使用终止进程的方式强制关闭正在运行的服务器  
参数 - callback 回调函数，如果不填写则使用同步请求，返回请求结果。如果填写则发送异步请求，通过回调函数传回调用结果，传回结果同函数返回值一致  
返回值: 返回{status: status,msg: msg}对象，成功时status=true，且无msg字段，失败时msg字段如下  
*        SERVERNOTRUN  = 服务器不在运行，无法关闭  
*        NOTLOGIN      = 没有登录账号，接口调用失败  
*        NETERROR      = 网络错误，接口请求失败  

---

函数名称 [webmcs.server.sendMessage]  
函数介绍: 发送文本消息给服务器  
参数 - message 欲发送给服务器的文本消息，如 help list 之类的Minecraft服务器指令  
参数 - callback 回调函数，如果不填写则使用同步请求，返回请求结果。如果填写则发送异步请求，通过回调函数传回调用结果，传回结果同函数返回值一致  
返回值: 返回{status: status,msg: msg}对象，成功时status=true，且无msg字段，失败时msg字段如下  
*        SERVERNOTRUN  = 服务器不在运行，无法发送消息  
*        UNKNOWNERROR  = 后台IO异常，一般不会出现这个错误，如果出现请检查报错并反馈给作者改进  
*        NOTLOGIN      = 没有登录账号，接口调用失败  
*        NETERROR      = 网络错误，接口请求失败  
*  

---

函数名称 [webmcs.server.restart]  
函数介绍: 通知后台重启服务器，请求后连接会被服务器挂起，直到重启成功或失败才会返回，请使用异步调用本接口  
参数 - callback 回调函数，如果不填写则使用同步请求，返回请求结果。如果填写则发送异步请求，通过回调函数传回调用结果，传回结果同函数返回值一致  
返回值: 返回{status: status,msg: msg}对象，成功时status=true，且无msg字段，失败时msg字段如下  
*        SERVERNOTRUN  = 服务器不在运行，无法进行重启  
*        TIMEOUT       = 服务器超时，无法重启  
*        NOTLOGIN      = 没有登录账号，接口调用失败  
*        NETERROR      = 网络错误，接口请求失败  
*  

---

