;(function(webmcs, layer){
  var version = '1.1.3.3';
  var versionCode = 1133;
  var updateMsgMap = {
    1133  : '<br>版本1.1.3.3<br>'+
            '1.修复了1.1.3.2一个手抖写出来的无法关闭服务器的bug',
    1132  : '<br>版本1.1.3.2<br>'+
            '1.重新调整了服务器启动逻辑，在网页开服后除非点击关服/强制关服按钮/计划任务触发关闭服务器三种情况，否则服务器均不会退出，无论是在服务器内输入stop指令还是服务器因插件崩溃或者其他原因关闭，WebMCS都会自动重启服务器',
    1131  : '<br>版本1.1.3.1<br>'+
            '1.修复了设置-Java附加参数无效的bug<br>'+
            '2.修复了高于1.9版本minecraft server中文显示乱码的bug<br>'+
            '3.为计划任务增加了执行程序功能，可以在指定时间执行一个自定义程序',
    1130  : '<br>版本1.1.3：<br>'+
            '1.改进了计划任务，从时间倒计时升级到Cron表达式<br>'+
            '2.修改了页面的一些细节问题，支持反向代理webmcs到某个虚拟子目录下<br>'+
            '3.优化版本检测机制，新版本更新检测更加及时<br><br>'+
            '【警告】本次更新会导致原计划任务丢失！更新前请确保没有需要备份的计划任务记录！',
    1121  : '<br>版本1.1.2.1：<br>'+
            '【严重】修复了一个路径注入漏洞，这个漏洞允许在拥有后台管理账号的前提下篡改运行WebMCS计算机/服务器上的文件！<br>' +
            '<br>'+
            '友情提示：鉴于Java权限过大，在泄露后台管理账号的前提下计算机/服务器的安全都无法进行保障，如果在服务器上使用，请务必通过安全工具隔离运行WebMCS（如虚拟机内运行,Sandboxie,Docker等）！<br>'+
            '虽然如果不泄露账号这个漏洞没有影响，但是依然强烈建议进行更新！',
    1120  : '<br>版本1.1.2：<br>'+
            '1.增加记住一周登陆状态功能<br>' +
            '2.修复部分浏览器缓存导致程序功能异常的bug<br>'+
            '3.修复因上个版本引入的中文乱码bug<br>'+
            '4.增强缓存管理，避免WebMCS版本更新后可能出现的微妙bug<br>' +
            '5.加入了版本更新提醒功能',
    1110  : '<br>版本1.1.1：<br>'+
            '1.修复了中文文件名上传乱码的bug<br>' +
            '2.增强了对于CSRF攻击的防御',
    1100  : '<br>版本1.1：<br>'+
            '1.对后端代码重新实现，部分组件更换为更完善的开源实现，性能和安全性都略有提高<br>' +
            '2.重写前端，整个网页实现方式重写，修改更加容易<br>' +
            '3.服务器存档备份压缩格式从zip改成7z，跨系统压缩不会乱码，压缩率更高<br>' +
            '4.完善网页在线编辑文件功能，支持网页文件上传、复制、移动、删除、压缩、下载等基本操作，支持网页直接编辑配置文件',
    1000  : '<br>版本1.0：<br>'+
            'WebMCS正式发布了~'
  };
  var updateMsg = '<div style="text-align: left;">';
  for (var key in updateMsgMap){
    if (key > webmcs.getVersionCode()){
      updateMsg += updateMsgMap[key];
    }
  }
  updateMsg += '</div>';
  var url = 'http://www.mcbbs.net/forum.php?mod=viewthread&tid=627752';
  var noPromptVersion = localStorage['_webmcs_update'];
  if (webmcs.getVersionCode() < versionCode && noPromptVersion != versionCode){
    if (webmcs.getVersionCode() <= 1121){
      //因为不小心引入了一些bug，旧版本采用confirm弹窗
      if(confirm("WebMCS检测到了新版本[" + version + "]，更新说明如下：\n\n" + updateMsg.replace(/<br>/g, '\n').replace(/<[^>]*>/g, '') + "\n\n是否确认更新？")){
        window.open(url);
      }else{
        if(confirm("是否不再提醒" + version + "更新？")){
          localStorage['_webmcs_update'] = versionCode;         
        }
      }
    }else{
      layer.open({
        content: 'WebMCS检测到了新版本[' + version + ']，更新说明如下：<br>' + updateMsg,
        btn: ['更新', '不更新'],
        yes: function(index){
          window.open(url);
          layer.closeAll();
        },
        no: function(){
          layer.open({
              content: '是否关闭' + version + '版本的更新提醒？',
              btn: ['关闭提醒', '不关闭'],
              yes: function(){
                localStorage['_webmcs_update'] = versionCode;
                layer.closeAll();
              }
            }
          );
        }
      });
    }
  }
}(webmcs, layer))