/**
 * 一个以实现简易为主的模板引擎，随便瞎写着玩儿玩儿的，以后看情况可能会迁移到Vue.js之类更专业的框架上，看着玩儿玩儿就好
 **/
(function($){
    if ($ == undefined){
        throw("missing jquery");
    }
    var templateCache = {};
    /**
     * 根据模板代码获取合并数据
     * 
     * template 模板代码，支持<% js代码 %>
     *          在js代码中可通过print或者简写p进行输出，如<% print('Hello world!'); %> <% p('Hello world!'); %>
     * $.et.load(url)来加载其他页面并进行模板渲染，如<% $.et.load('1.template') %>
     * data 数据对象，在模板中使用data对象即可引用内部内容
     **/
    var et = {};
    et.getHtml = function(template, data){
       var code = mergeCode(template2Code(template), data);
        try{
            var html = eval(code);
            return html;
        }catch(e){
            console.log(code);
            console.log(e);
            return null;
        }
    }
    et.load = function(url, data){
        var template = templateCache[url];
        if (template == undefined){
            $.ajax({
                type: 'get',
                url: url,
                async: false,
                success: function(html){
                    template = html;
                    templateCache[url] = html;
                },
                error: function(){
                    throw("ajax failed:" + url);
                }
            });
        }
        var code = 'var p = print;var data = ' + JSON.stringify(data) + ';' + template2Code(template);
        eval(code);
    }
    $.extend({et: et});
    var escapeText = function(text){
        text = text.replace(/\\/g, '\\\\');
        text = text.replace(/"/g, '\\"');
        text = text.replace(/\n/g,'\\n');
        text = text.replace(/\r/g, '');
        return text;
    }
    var template2Code = function(template){
        var code = '';
        var j = 0;
        while((i = template.indexOf('<%', j)) != -1){
            code += ';print("' + escapeText(template.substring(j, i)) + '");';
            i += 2;
            j = template.indexOf('%>', i);
            code += template.substring(i, j);
            j += 2;
        }
        code += ';print("' + escapeText(template.substring(j)) + '");';
        return code;
    }
    var mergeCode = function(code, data){
         var tmp =  '(function(){var _tmp = "";' +
                    'var p = print = function(text){' + 
                    '   _tmp += text;' +
                    '};' + 
                    'var data = ' + JSON.stringify(data) + ';';
        tmp += code;
        tmp += ';return _tmp;}());';
        return tmp;
    }
}(jQuery));
