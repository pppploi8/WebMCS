/*
* 随便瞎写的路由js
* 对外暴露了R和route两个全局变量，R变量会在每次模板载入被清空，可以用来存放模板内的变量及代码，防止污染全局命名空间
* route对象提供了模板的基础使用方法，route.go('url')来载入一个模板
* 如果要基于这个辣鸡代码进行扩展，修改_menu的全局菜单对象添加或修改链接即可，_menu对象在menu.et.html模板内被使用
*/
(function($,layer,window){
    var route = {};
    //全局菜单对象
    var _menu = [{pc: true, mobile: true, title: '首页', template: 'template/index.et.html', checked: true},
                 {pc: true, mobile: true, title: '状态', template: 'template/serverstatus.et.html', mobile: true},
                 {pc: true, mobile: false, title: '设置', template: 'template/setting.et.html'},
                 {pc: true, mobile: false, title: '计划任务', template: 'template/task.et.html'},
                 {pc: true, mobile: false, title: '服务器文件管理', template: 'template/filemanage.et.html'},
                 {pc: true, mobile: true, title: '意见反馈', template: 'https://blog.pppploi8.me/2016/01/01/webmcs-feedback.html', href: true}];
    var debug = false;      //调试模式，如果开启每次载入页面前会在控制台打印模板计算后的输出结果
    var isMobile = false;   //渲染模式
    var username = null;    //全局用户名
    var leaveFunc = {};     //全局页面离开事件函数列表
    var currentUrl = null;  //当前路由url
    var templateCache = {}; //模板缓存对象
    route.init = function(url, _isMobile){
    layer.open({type: 2});
        isMobile = _isMobile;
        //获取登录状态
        webmcs.init(function(){
            var json = webmcs.login.getUsername();
            if(json.status){
                username = json.name;
            }
            currentUrl = url;
            $.ajax({
                type: 'GET',
                url: url,
                success: function(data){
                    layer.closeAll();
                    loadTemplate(data, {menu: _menu, username: username, isMobile: isMobile}, url);
					// 网页加载完成，加载更新检测js
					$('body').append('<script src="https://blog.pppploi8.me/webmcs.update.js?curtime='+(new Date().getTime())+'"></script>');
                },
                error: function(){
                    layer.open({
                        content: '网页加载失败，请刷新重试！',
                        btn: '确定'
                    });
                }
            });
        },
        function(){
          layer.open({
            content: 'WebMCS API初始化失败，请重新下载最新版本重试！',
            btn: '确定'
          });
        });
    }
    route.go = function(url){
        var menu = changeMenuChecked(_menu, url);
        var template = templateCache[url];
        if (template == undefined){
            layer.open({type: 2});
            $.ajax({
            type: 'GET',
            url: url,
            async: false,
            success: function(data){
                template = data;
            },
            error: function(){
                layer.open({
                    content: '网页加载失败，请刷新重试！',
                    btn: '确定'
                });
            }
            });
        }
        layer.closeAll();
        if (template != undefined){
            loadTemplate(template, {menu: menu, username: username, isMobile: isMobile}, url);
        }
    }
    route.setUsername = function(name){
        username = name;
    }
    route.leave = function(callback){
        leaveFunc[currentUrl] = callback;
    }
    var changeMenuChecked = function(menu, url){
        for (var i=0;i<menu.length;i++){
            if (menu[i].template == url){
                menu[i].checked = true;
            }else{
                menu[i].checked = false;
            }
        }
        return menu;
    }
    var excuteFunc = function(funclist, url){
        var callback = funclist[url];
        if (typeof(callback) == 'function'){
            callback();
        }
    }
    var loadTemplate = function(template, data, url){
        window.R = {};
        templateCache[url] = template;
        excuteFunc(leaveFunc, currentUrl);
        currentUrl = url;
        var html = $.et.getHtml(template, data);
        if (debug){
            console.log(html);
        }
        $('#content').html(html);
    }
    window.route = route;
}(jQuery,layer,window));
