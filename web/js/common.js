(function($, layer){
    var common = {};
    common.showMsg = function(msg){
        layer.open({
            content: msg,
            skin: 'msg',
            time: 2
        });
    }
    window.common = common;
    // 为IE提供endsWith和startsWith支持
    if (typeof String.prototype.startsWith != 'function') {  
      String.prototype.startsWith = function (prefix){
        return this.slice(0, prefix.length) === prefix;
      };  
    }
    if (typeof String.prototype.endsWith != 'function'){
      String.prototype.endsWith = function (suffix){
        return this.substring(this.length - suffix.length) === suffix;
      }
    }
}(jQuery, layer));