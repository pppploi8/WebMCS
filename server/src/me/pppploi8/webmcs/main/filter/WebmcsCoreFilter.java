package me.pppploi8.webmcs.main.filter;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import me.pppploi8.webmcs.http.jetty.HttpFilter;

/**
 * 这个类负责核心的登陆校验
 * @author pppploi8
 */
public class WebmcsCoreFilter implements HttpFilter{

	@Override
	public boolean handle(HttpServletRequest request, HttpServletResponse response) {
		Object username = request.getSession().getAttribute("username");
        if (username == null || username.equals("")){
        	//没有找到登陆状态记录， 拒绝任何操作
            try {
            	response.getOutputStream().write("{\"status\":false,\"msg\":\"NOTLOGIN\"}".getBytes());
            	response.getOutputStream().flush();
            } catch (Exception e) { }
            return false;
        }
        return true;//继续操作
	}

}
