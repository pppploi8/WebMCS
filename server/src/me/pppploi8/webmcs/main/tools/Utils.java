package me.pppploi8.webmcs.main.tools;

import java.util.Map;
import java.util.TreeMap;

public abstract class Utils {
	/**
	 * 过滤路径，删除路径中的../来避免被恶意注入到上级目录
	 * @param path 原始路径
	 * @return 转换后路径
	 */
	public static String filterPath(String path){
		if (path == null){
			return "/";
		}
		path = path.replace("\\", "/");
		if (!path.startsWith("/")){
			path = "/" + path;
		}
		while(path.indexOf("/../") != -1){
			path = path.replace("/../", "/");
		}
		if (path.endsWith("/..")){
			path = "/";
		}
		return path;
	}
	/**
	 * 根据路径获取文件名
	 * @param path 路径
	 * @return
	 */
	public static String getFilenameByPath(String path){
		path = path.replace("\\", "/");
		int i = path.lastIndexOf("/");
		if (i == -1){
			return path;
		}else{
			return path.substring(i+1);
		}
	}
	public static boolean stringIsNotBlank(String s){
		return !stringIsBlank(s);
	}
	public static boolean stringIsBlank(String s){
		return s==null||"".equals(s);
	}
	public static boolean arrayIsNotBlank(Object[] o){
		return !arrayIsNotBlank(o);
	}
	public static boolean arrayIsBlank(Object[] o){
		return o==null||o.length==0;
	}
	/**
	 * 创建一个用于返回JSON的MAP对象
	 */
	public static Map<String, Object> getResultMap(){
		Map<String, Object> map = new TreeMap<>();
		map.put("status", false);
		return map;
	}
	/**
	 * 检测参数是否为空
	 * @param ret 返回用Map对象
	 * @param path 参数
	 * @return
	 */
	public static boolean checkParamIsBlank(Map<String, Object> ret, Object ... param) {
		for(Object o : param){
			if (o.getClass().isArray()){
				if (arrayIsBlank((Object[])o)){
					ret.put("msg", "PARAMERROR");
					return true;
				}
			}else{
				if (stringIsBlank((String)o)){
					ret.put("msg", "PARAMERROR");
					return true;
				}
			}
		}
		return false;
	}
}
