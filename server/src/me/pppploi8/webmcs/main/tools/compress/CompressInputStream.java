package me.pppploi8.webmcs.main.tools.compress;

import java.io.IOException;
import java.io.InputStream;

public abstract class CompressInputStream extends InputStream{
	public abstract CompressEntry getNextEntry() throws IOException;
}
