package me.pppploi8.webmcs.main.tools.compress.impl;

import java.io.IOException;

import org.apache.commons.compress.archivers.ArchiveEntry;
import org.apache.commons.compress.archivers.sevenz.SevenZFile;

import me.pppploi8.webmcs.main.tools.compress.CompressEntry;
import me.pppploi8.webmcs.main.tools.compress.CompressInputStream;

public class SevenZInputStream extends CompressInputStream {
	private SevenZFile sevenZFile;

	public SevenZInputStream(SevenZFile sevenZFile) {
		this.sevenZFile = sevenZFile;
	}

	@Override
	public CompressEntry getNextEntry() throws IOException {
		ArchiveEntry entry = sevenZFile.getNextEntry();
		return entry == null?null:new SevenZEntry(entry);
	}

	@Override
	public int read() throws IOException {
		return sevenZFile.read();
	}
	
	@Override
	public void close() throws IOException{
		sevenZFile.close();
	}
}