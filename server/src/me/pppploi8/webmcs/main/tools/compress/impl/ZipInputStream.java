package me.pppploi8.webmcs.main.tools.compress.impl;

import java.io.IOException;

import org.apache.commons.compress.archivers.ArchiveEntry;
import org.apache.commons.compress.archivers.ArchiveInputStream;

import me.pppploi8.webmcs.main.tools.compress.CompressEntry;
import me.pppploi8.webmcs.main.tools.compress.CompressInputStream;

public class ZipInputStream extends CompressInputStream{
	private ArchiveInputStream ais;
	
	public ZipInputStream(ArchiveInputStream ais){
		this.ais = ais;
	}
	
	@Override
	public CompressEntry getNextEntry() throws IOException {
		ArchiveEntry entry = ais.getNextEntry();
		return entry == null?null:new ZipEntry(entry);
	}

	@Override
	public int read() throws IOException {
		return ais.read();
	}
	
	@Override
	public void close() throws IOException {
		ais.close();
	}

}
