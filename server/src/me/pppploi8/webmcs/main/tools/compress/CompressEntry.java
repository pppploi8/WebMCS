package me.pppploi8.webmcs.main.tools.compress;

public interface CompressEntry {
	public String getName();
	public boolean isDirectory();
}
