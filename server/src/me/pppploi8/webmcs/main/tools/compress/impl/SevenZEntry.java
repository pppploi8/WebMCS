package me.pppploi8.webmcs.main.tools.compress.impl;

import org.apache.commons.compress.archivers.ArchiveEntry;

import me.pppploi8.webmcs.main.tools.compress.CompressEntry;

public class SevenZEntry implements CompressEntry{
	private ArchiveEntry entry;

	public SevenZEntry(ArchiveEntry entry){
		this.entry = entry;
	}
	
	@Override
	public String getName() {
		return entry.getName();
	}

	@Override
	public boolean isDirectory() {
		return entry.isDirectory();
	}
}
