package me.pppploi8.webmcs.main.urlmapping;

import java.util.UUID;

import me.pppploi8.webmcs.http.jetty.HttpUrlMapping;

public class WebmcsSecurityService extends HttpUrlMapping{
	public String getToken(){
		String token = UUID.randomUUID().toString().replace("-", "");
		getRequest().getSession().setAttribute("token", token);
		return token;
	}
}
