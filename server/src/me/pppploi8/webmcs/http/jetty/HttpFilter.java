package me.pppploi8.webmcs.http.jetty;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public interface HttpFilter {
	/**
	 * 
	 * @param request
	 * @param response
	 * @return boolean true=执行完毕向后放行，false=执行完毕中断后续过滤器链的传递
	 */
	public boolean handle(HttpServletRequest request,HttpServletResponse response);
}
