package me.pppploi8.webmcs.task.impl;

import me.pppploi8.webmcs.task.TaskCore;

/**
 * 无任何作用的空对象，用于异常时使用的对象
 * @author pppploi8
 *
 */
public class TaskNull extends TaskCore{
	private static TaskNull task = new TaskNull();
	
	private TaskNull(){}
	public static TaskNull getInstance(){
		return task;
	}
	@Override
	public TaskCore exec() {
		return this;
	}
	@Override
	public int getType() {
		return 0;
	}
}
