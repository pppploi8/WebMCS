package me.pppploi8.webmcs.task.impl;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Date;

import me.pppploi8.webmcs.main.tools.CompressUtils;
import me.pppploi8.webmcs.task.TaskCore;

public class TaskBak extends TaskCore{
	private static SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddHHmmss");

	@Override
	public TaskCore exec() {
		try{
			Date date = new Date();
			File file = new File(msc.getServerPath().getAbsolutePath() + "/backup/" + sdf.format(date) + ".7z" );
			file.getParentFile().mkdirs();
			CompressUtils.compressToSevenZ(new File[]{new File(msc.getServerPath() + "/world")}, file);
		}catch(Exception e){
			e.printStackTrace();
		}
		return this;
	}
	
	@Override
	public int getType() {
		return 1;
	}

}
