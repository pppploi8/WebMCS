package me.pppploi8.webmcs.task.impl;

import me.pppploi8.webmcs.task.TaskCore;

public class TaskCommond extends TaskCore{
	
	@Override
	public TaskCore exec() {
		this.msc.sendMessage(content);
		return this;
	}
	
	@Override
	public int getType() {
		return 2;
	}
}
