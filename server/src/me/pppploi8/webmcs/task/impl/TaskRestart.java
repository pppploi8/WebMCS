package me.pppploi8.webmcs.task.impl;

import me.pppploi8.webmcs.task.TaskCore;

public class TaskRestart extends TaskCore{

	@Override
	public TaskCore exec() {
		msc.restart();
		return this;
	}
	@Override
	public int getType() {
		return 1;
	}

}
