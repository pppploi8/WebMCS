package me.pppploi8.webmcs.task.impl;

import me.pppploi8.webmcs.task.TaskCore;

public class TaskStart extends TaskCore{

	@Override
	public TaskCore exec() {
		msc.start();
		return this;
	}
	@Override
	public int getType() {
		return 1;
	}

}
