package me.pppploi8.webmcs.task.impl;

import me.pppploi8.webmcs.task.TaskCore;

public class TaskStop extends TaskCore{

	@Override
	public TaskCore exec() {
		msc.stop();
		return this;
	}
	@Override
	public int getType() {
		return 1;
	}
}
