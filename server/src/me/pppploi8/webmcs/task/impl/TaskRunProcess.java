package me.pppploi8.webmcs.task.impl;

import me.pppploi8.webmcs.task.TaskCore;

import java.io.IOException;

/**
 * 执行进程计划任务，负责定时运行一个外部进程
 */
public class TaskRunProcess extends TaskCore {

    @Override
	public TaskCore exec() {
		Process process = null;
		try {
			process = Runtime.getRuntime().exec(super.content);
			process.wait();
		} catch (IOException|InterruptedException e) {
			e.printStackTrace();
		}
		return this;
    }
    
	@Override
	public int getType() {
		return 3;
	}
}