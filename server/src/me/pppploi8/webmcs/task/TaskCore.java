package me.pppploi8.webmcs.task;

import me.pppploi8.webmcs.minecraft.server.core.McServerCore;
import org.quartz.spi.MutableTrigger;

/**
 * 计划任务核心类，每个任务的实现都必须继承这个类
 * @author pppploi8
 *
 */
public abstract class TaskCore {
	private String id;
	private String time;
	private String execType;
	private MutableTrigger mutableTrigger;
	private long nextRuntime;
	protected String content;
	protected McServerCore msc;
	
	public String getExecType() {
		return execType;
	}
	public void setExecType(String execType) {
		this.execType = execType;
	}
	public String getContent(){
		return content;
	}
	public TaskCore setContent(String content) {
		this.content = content;
		return this;
	}
	public String getTime(){
		return time;
	}
	public TaskCore setTime(String time){
		this.time = time;
		return this;
	}
	public TaskCore setMcServerCore(McServerCore msc){
		this.msc = msc;
		return this;
	}
	public TaskCore setId(String id){
		this.id = id;
		return this;
	}
	public String getId() {
		return id;
	}
    public MutableTrigger getMutableTrigger() {
        return mutableTrigger;
    }
    public void setMutableTrigger(MutableTrigger mutableTrigger) {
        this.mutableTrigger = mutableTrigger;
    }
    public long getNextRuntime() {
        return nextRuntime;
    }
    public void setNextRuntime(long nextRuntime) {
        this.nextRuntime = nextRuntime;
	}
	
	/**
	 * 执行任务，每个计划任务必须实现自己的版本
	 * @return
	 */
	public abstract TaskCore exec();
	public abstract int getType();
}
