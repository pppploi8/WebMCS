package me.pppploi8.webmcs.minecraft.server.struct;

/**
 * MC消息接口，如果要接受消息需要实现这个接口
 * @author pppploi8
 */
public interface McMessageInterface {
    void message(String message);
}
