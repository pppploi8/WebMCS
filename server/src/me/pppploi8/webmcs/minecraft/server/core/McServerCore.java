package me.pppploi8.webmcs.minecraft.server.core;

import java.io.File;
import me.pppploi8.webmcs.minecraft.server.struct.McMessageInterface;

public interface McServerCore {

	/**
	 * 设置服务器重启超时时间，如果超过这个时间服务器仍未关闭，则强制关闭服务器
	 * @param timeout 单位为秒
	 * @return 返回对象自身
	 */
	McServerCore setRestartTimeOut(long timeout);

	/**
	 * 获取服务端jar文件，可用于检测是否存在
	 * @return 
	 */
	File getJarFile();

	/**
	 * 设置java路径，用于启动服务端，如果不填写默认使用java
	 * @param javaPath java路径
	 * @return 返回独享自身
	 */
	McServerCore setJavaPath(String javaPath);

	/**
	 * 设置jar文件路径，如果不设置默认为运行目录下server\start.jar
	 * @param filePath jar文件路径
	 * @return 返回对象自身
	 */
	McServerCore setJarFile(String filePath);

	McServerCore setStartParam(String startParam);

	McServerCore setMaxMemory(String maxMemory);

	/**
	 * 设置服务端运行目录
	 * @param path 如果不设置默认为运行目录下的server
	 * @return 
	 */
	McServerCore setServerPath(String path);

	/**
	 * 设置一个回调用来接受服务端发送的消息
	 * @param mm 回调接受接口
	 * @return 返回对象自身
	 */
	McServerCore setMessageNotice(McMessageInterface mm);

	/**
	 * 设置自定义java启动命令行
	 * @param javaParam java启动参数，会被加入到 java之后，-jar 'jar服务端文件'之前的位置
	 * @return 返回对象自身
	 */
	McServerCore setJavaParam(String javaParam);

	/**
	 * 启动服务端
	 * @return 如果进程启动成功返回true，否则返回false，无法重复运行，如果重复运行直接返回false
	 */
	boolean start();

	/**
	 * 发送命令给服务端，不代表服务端一定能响应命令（比如特殊的异常卡死服务端不退出）
	 * @param message 命令
	 * @return 
	 */
	boolean sendMessage(String message);

	/**
	 * 获取服务器运行路径
	 * @return
	 */
	File getServerPath();

	/**
	 * 获取服务器进程存活状态
	 * @return 进程运行返回true，否则返回false
	 */
	boolean getServerStatus();

	/**
	 * 通过发送stop指令关闭服务端
	 * @return 指令发送成功或者失败
	 */
	boolean stop();

	/**
	 * 强制终止服务端进程
	 * @return 
	 */
	boolean termination();

	/**
	 * 重启服务端，这是一个同步方法，会造成线程的挂起，请注意
	 * @param timeout 超时时间，单位为秒，超过这个时间会直接使用终止进程的方式关闭服务端。如果为负数则使用默认值5分钟
	 * @return 成功执行或失败，不保证服务端一定重启完毕
	 */
	boolean restart();

}