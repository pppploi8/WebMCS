package me.pppploi8.webmcs.minecraft.server.core;

import java.io.File;
import java.io.FileInputStream;
import java.util.Properties;

import me.pppploi8.webmcs.minecraft.server.core.impl.McServerCoreImpl;

/**
 * McServer工厂，可以通过修改这个类根据session获取不同的McServer对象来实现多用户支持，WebMCS因安全性问题无法解决暂不打算提供多用户管理功能
 * @author pppploi8
 *
 */
public abstract class McServerFactory {
	private static McServerCore mcServerCore;
	
	public static McServerCore createMcServer(String curPath){
		if (mcServerCore == null){
			McServerCore mcs = new McServerCoreImpl()
	                .setServerPath(curPath + "/server")
	                .setJarFile("start.jar");
	        //读取配置文件初始化McServerCore对象
	        try {
	            File file = new File(curPath + "/config.properties");
	            Properties p = new Properties();
	            p.load(new FileInputStream(file));
	            mcs.setJavaPath(p.getProperty("javaPath"))
	               .setJarFile(p.getProperty("jarPath"))
	               .setJavaParam(p.getProperty("javaParam"))
	               .setStartParam(p.getProperty("startParam"))
	               .setMaxMemory(p.getProperty("maxMemory"));
	        } catch (Exception e) { }
	        mcServerCore = mcs;
		}
		return mcServerCore;
	}
	
	public static McServerCore getMcServerCore(){
		return mcServerCore;
	}
}
